import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};

        printBoard(board);
        while (true) {

            while (true) {
                System.out.print("Player 1 enter row number:");
                int row = reader.nextInt();
                System.out.print("Player 1 enter column number:");
                int col = reader.nextInt();
                if (row > 3 || col > 3 || row < 1 || col < 1) {
                    System.out.println("Enter number from 1 to 3");
                } else if (board[row - 1][col - 1] == 'X' | board[row - 1][col - 1] == 'O') {
                    System.out.println("Full");
                } else {
                    board[row - 1][col - 1] = 'X';

                    printBoard(board);
                    break;
                }
            }
            if (checkboard('X',board) == true){
                System.out.println("Player 1 WON!");
                break;
            }
            boolean bool = false;
            for (int i = 0; i <= 2; i++) {
                for (int x = 0; x <= 2; x++) {
                    char test = board[i][x];
                    if (test == ' ') {
                        bool = true;
                    }
                }
            }
            if (bool == false) {
                System.out.println("CAT!");
                break;
            }
            while (true) {
                System.out.print("Player 2 enter row number:");
                int row = reader.nextInt();
                System.out.print("Player 2 enter column number:");
                int col = reader.nextInt();
                if (row > 3 || col > 3 || row < 1 || col < 1) {
                    System.out.println("Enter number from 1 to 3");
                } else if (board[row - 1][col - 1] == 'X' | board[row - 1][col - 1] == 'O') {
                    System.out.println("Full");
                } else {
                    board[row - 1][col - 1] = 'O';
                    printBoard(board);
                    break;
                }
            }
            if (checkboard('O',board) == true){
                System.out.println("Player 2 WON!");
                break;
            }
        }
    }
    public static void printBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");
            }
            System.out.println();
            System.out.println("   -----------");

        }
    }

    public static boolean checkboard(char symbol, char[][] board) {
        if ((board[0][0] == symbol) & (board[0][1] == symbol) & (board[0][2] == symbol)){
            return true ;

        } else if ((board[1][0] == symbol) & (board[1][1] == symbol) & (board[1][2] == symbol)){
            return true ;
        } else if ((board[2][0] == symbol) & (board[2][1] == symbol) & (board[2][2] == symbol)) {
            return true ;
        } else if ((board[0][0] == symbol) & (board[1][0] == symbol) & (board[2][0] == symbol)) {
            return true ;
        } else if ((board[0][1] == symbol) & (board[1][1] == symbol) & (board[2][1] == symbol)) {
            return true ;
        } else if ((board[0][2] == symbol) & (board[1][2] == symbol) & (board[2][2] == symbol)) {
            return true ;
        } else if ((board[0][0] == symbol) & (board[1][1] == symbol) & (board[2][2] == symbol)) {
            return true;
        } else if ((board[0][2] == symbol) & (board[1][1] == symbol) & (board[2][0] == symbol)){
            return true;
        }

        return false;
    }
}